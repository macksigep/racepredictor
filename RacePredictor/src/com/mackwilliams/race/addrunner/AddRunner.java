package com.mackwilliams.race.addrunner;

import java.util.Scanner;

public class AddRunner {
	
	public AddRunner(){
		
		
	}
	
	
	public static void addRunner(){
	    String rNameAdd = "null";
		String rBIBadd = "null";
		String rTimeAdd =  "null";
		Scanner s = new Scanner(System.in);
		System.out.println("Add Runner Name: ");
			rNameAdd = s.nextLine() ;
		System.out.println("Add Runner BIB Number: ");
			rBIBadd = s.nextLine() ;
		System.out.println("Add Runner Time: ");
			rTimeAdd = s.nextLine() ;
		sendToWriteFile(rNameAdd, rBIBadd, rTimeAdd);
		s.close();
	}
	
	private static void sendToWriteFile(String rna, String rba, String rta){
		WriteRunner wr = new WriteRunner(rna, rba, rta);
		addRunnerPrompt();}
	
	private static void addRunnerPrompt(){
		Scanner s = new Scanner(System.in);
		System.out.println("Add another athlete?");
		String input = s.nextLine();
		switch(input){
		case "yes":
		case "Yes":
				addRunner();
		case "no": 
		case "No":
				s = new Scanner(System.in);
				System.out.println("Are you sure?");
				input = s.nextLine();
				 if(input.equalsIgnoreCase("yes")){
					 System.out.println("Exited");
						System.exit(0);
					}else{addRunner();}
			}
		s.close();
	}

}
