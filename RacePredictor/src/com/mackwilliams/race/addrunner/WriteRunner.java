package com.mackwilliams.race.addrunner;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import com.mackwilliams.race.models.*;

public class WriteRunner {
	
	/*
	 * writes runners to an output file
	 */
	
	private String runnerName= "null";
	private String runnerBIB = "null";
	private String runnerTime =  "null";
	
	private static ArrayList<Runner> runners;

public WriteRunner(String rn, String rb, String rt){
	this.runnerName=rn;	this.runnerBIB=rb;	this.runnerTime=rt;
	 runners = new ArrayList<Runner>();
		runners.add(new Runner(runnerName,runnerBIB, runnerTime));
		writeRunners(runners);
}		
	
	public static void writeRunners(ArrayList<Runner>r){
		  String fileName= "runner.txt ";
		  PrintWriter outputFile=null;
		try {
			outputFile = new PrintWriter(new FileWriter(fileName, true));
			for(Runner str: runners) {outputFile.println(str);}
			}catch (IOException e){e.printStackTrace();}
		outputFile.close();}
	}

/* old write to file code from line 36 block
 *  // for(Runner str: runners) {
			//  outputFile.println(str);
			  //System.out.println(str);
 */
