package com.mackwilliams.race.predictor;

import java.text.DecimalFormat;

public class RacePredictor {

	public static void main(String[] args) {
		DecimalFormat dec = new DecimalFormat(".##");
	 	double raceType = 26.2;
		String time = "7:38";
		int dilim = time.indexOf(':');
		String min = time.substring(0, dilim);
		double calcFactor = Double.parseDouble(time.substring(dilim+1, time.length()));
		double timeFactor = calcFactor/60;
		//time with mins and sec as decimal ex. 7.63
		double timeString = Double.parseDouble(min.concat(dec.format(timeFactor)));
		//takes time as decimal and multiplies time as dec by race distance for total time in dec
		//7.63*26.2=199.63 minutes
	    double raceInMins =  timeString*raceType;
	    //converts mintues in decimal to hour and minutes format
	    //199.63/60.0 = 3.33 hours
	    double raceTimeDecimal = raceInMins/60.0;
	    //isolates the hour for its own variable and eventual output
	    //and returns quotient
	    // 3.33/1 = 3 hours
	    int hour = ((int)(raceTimeDecimal/1)) ;	   
	    //converts 3.33 to a string, "3.33" for substringing the minutes for output
	   String totalTimeDec = Double.toString(raceTimeDecimal);
	   //returns .33 as a substring from 3.33, then parses to a double for minutes calc
	   double minDec = Double.parseDouble(totalTimeDec.substring(1,totalTimeDec.length()));
	    //holds hour, 3, for output and formatting
		int hourOut = (int)hour;
		//holds minutes after calculating 0.33,  decimal to actual minutes from a decimal
		//.33 = 19
		double minutes = Math.round( (minDec * 60));
		int minutesOut = (int)(minutes);
		 System.out.println(String.format("%s(h) %s(m)", hourOut, minutesOut));

	}

}
